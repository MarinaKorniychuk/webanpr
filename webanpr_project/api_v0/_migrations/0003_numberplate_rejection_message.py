# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-01-21 13:17
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api_v0', '0002_numberplate_image'),
    ]

    operations = [
        migrations.AddField(
            model_name='numberplate',
            name='rejection_message',
            field=models.CharField(default=None, max_length=63, null=True),
        ),
    ]
