from django.conf.urls import url, include

urlpatterns = [
    url(r'^', include('api_v0.auth.urls')),
    url(r'^', include('api_v0.plates.urls')),
]
