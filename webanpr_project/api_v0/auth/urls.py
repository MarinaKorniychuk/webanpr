from django.conf.urls import url

from api_v0.auth.views import SignUpView, LogInView, LogOutView

urlpatterns = [
    url(r'^signup/$', SignUpView.as_view(), name='signup'),
    url(r'^login/$',  LogInView.as_view(), name='login'),
    url(r'^logout/$', LogOutView.as_view(), name='logout'),

]
