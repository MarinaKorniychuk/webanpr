from django.contrib.auth import login, logout
from django.contrib.auth.models import User
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response

from api_v0.auth.serializers import UserSerializer, LogInSerializer


class SignUpView(APIView):
    permission_classes = (AllowAny,)

    def post(self, request):
        serializer = UserSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        user = User.objects.create(
            email=serializer.validated_data['email'],
            username=serializer.validated_data['username']
        )

        user.set_password(serializer.validated_data['password'])
        user.save()

        return Response(UserSerializer(user).data, status=status.HTTP_201_CREATED)


class LogInView(APIView):
    permission_classes = (AllowAny,)

    def post(self, request):
        serializer = LogInSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        try:
            if 'username' in serializer.validated_data:
                user = User.objects.get(username=serializer.validated_data['username'])
            else:
                user = User.objects.get(email=serializer.validated_data['email'])
        except User.DoesNotExist:
            return Response(status=status.HTTP_422_UNPROCESSABLE_ENTITY)

        if not user.check_password(serializer.validated_data['password']):
            return Response(status=status.HTTP_422_UNPROCESSABLE_ENTITY)

        login(request, user)

        return Response(UserSerializer(user).data)


class LogOutView(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        logout(request)
        return Response()
