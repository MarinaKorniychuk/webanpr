from django.contrib.auth.models import User
from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from rest_framework.validators import UniqueValidator


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('username', 'email', 'password')
        extra_kwargs = {
            'password': {
                'write_only': True,
                'min_length': 6,
                'max_length': 127
            },
            'email': {
                'validators': [UniqueValidator(queryset=User.objects.all())]
            },
            'username': {
                'validators': [UniqueValidator(queryset=User.objects.all())]
            }
        }


class LogInSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('username', 'email', 'password')
        extra_kwargs = {
            'username': {
                'required': False,
                'validators': []
            },
            'email': {
                'required': False
            }
        }

    def validate(self, data):
        if 'username' in data and 'email' in data or 'username' not in data and 'email' not in data:
            raise ValidationError('Either email or username must be passed')
        return data
