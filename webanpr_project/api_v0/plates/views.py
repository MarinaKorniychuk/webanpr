from PIL import Image
from django.contrib.auth.models import User
from django.shortcuts import render, get_object_or_404
from rest_framework import status
from rest_framework.exceptions import MethodNotAllowed
from rest_framework.generics import ListCreateAPIView, ListAPIView
from rest_framework.permissions import IsAuthenticated, AllowAny, IsAdminUser
from rest_framework.response import Response
from rest_framework.views import APIView

from api_v0.plates.models import NumberPlate
from api_v0.plates.serializers import PlateSerializer, ModerateSerializer, ImageSerializer


class HomeView(APIView):

    def get(self, request):
        data = dict()
        data['count_numberplates'] = NumberPlate.objects.all().count()
        data['count_users'] = User.objects.all().count()

        return Response(data)


class PlatesView(ListCreateAPIView):
    serializer_class = PlateSerializer

    def dispatch(self, request, *args, **kwargs):
        self.filter_type = self.kwargs.get('filter')

        return super(PlatesView, self).dispatch(request, *args, **kwargs)

    def get_permissions(self):
        if self.request.method == 'POST':
            self.permission_classes = (IsAuthenticated,)

            # Disallow POST method to /plates/{{something}} urls.
            if self.filter_type is not None:
                raise MethodNotAllowed(self.request.method)
        else:
            if self.filter_type is None:
                self.permission_classes = (AllowAny,)
            elif self.filter_type == 'own':
                self.permission_classes = (IsAuthenticated,)
            elif self.filter_type == 'not-moderated':
                self.permission_classes = (IsAdminUser,)

        return super(PlatesView, self).get_permissions()

    def get_queryset(self):
        if self.filter_type is None:
            return NumberPlate.objects.all().filter(is_accepted=True)
        elif self.filter_type == 'own':
            return NumberPlate.objects.all().filter(user=self.request.user)
        elif self.filter_type == 'not-moderated':
            return NumberPlate.objects.all().filter(is_moderated=False)

    def create(self, request, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)

        number_plate = NumberPlate.objects.create(
            user=User.objects.get(id=request.user.id),
            number=serializer.validated_data['number'],
            image=serializer.validated_data['image'],
        )
        number_plate.save()

        return Response(self.serializer_class(number_plate).data, status=status.HTTP_201_CREATED)


class ModeratePlateView(APIView):
    permission_classes = (IsAdminUser,)

    def post(self, request, plate_id, action):
        plate = get_object_or_404(NumberPlate, id=plate_id)

        if plate.is_moderated:
            return Response({'message': 'Plate is already moderated'}, status=status.HTTP_400_BAD_REQUEST)

        if action == 'accept':
            plate.is_accepted = True
        elif action == 'reject':
            serializer = ModerateSerializer(data=request.data)
            serializer.is_valid(raise_exception=True)

            plate.rejection_message = serializer.validated_data['message']

        plate.is_moderated = True
        plate.save()

        return Response()


class RecognizePlateView(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        serializer = ImageSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        img = Image.open(serializer.validated_data['image']).convert('RGB')
        pixel_count = img.height * img.width
        color_string = '#'

        for band in range(3):
            color_string += hex(round(sum(list(img.getdata(band))) / pixel_count))[2:]

        return Response({'number': color_string})
