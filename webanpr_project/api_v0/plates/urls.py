from django.conf.urls import url

from api_v0.plates.views import HomeView, PlatesView, ModeratePlateView, RecognizePlateView

urlpatterns = [
    url('^home/$',                                                  HomeView.as_view(),             name='home'),
    url('^plates/$',                                                PlatesView.as_view(),           name='plates'),
    url('^plates/(?P<filter>(own|not-moderated))/$',                PlatesView.as_view()),
    url('^plates/(?P<plate_id>\d+)/(?P<action>(accept|reject))/$',  ModeratePlateView.as_view(),    name='moderate'),
    url('^plates/recognize/$',                                      RecognizePlateView.as_view(),   name='recognize'),
]
