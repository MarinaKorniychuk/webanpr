from django.db import models
from django.contrib.auth.models import User


class NumberPlate(models.Model):
    user = models.ForeignKey(User)
    number = models.CharField(max_length=12)
    image = models.ImageField(default=None)
    is_moderated = models.BooleanField(default=False)
    is_accepted = models.BooleanField(default=False)
    rejection_message = models.CharField(max_length=63, default=None, null=True)
