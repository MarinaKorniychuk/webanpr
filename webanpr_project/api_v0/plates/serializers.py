from rest_framework import serializers
from rest_framework.exceptions import ValidationError


class PlateSerializer(serializers.Serializer):

    number = serializers.CharField()
    image = serializers.ImageField()
    is_accepted = serializers.BooleanField(read_only=True)
    is_moderated = serializers.BooleanField(read_only=True)


class ModerateSerializer(serializers.Serializer):

    message = serializers.CharField(max_length=63)


class ImageSerializer(serializers.Serializer):

    image = serializers.ImageField()
