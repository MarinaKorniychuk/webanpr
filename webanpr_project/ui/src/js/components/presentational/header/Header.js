import React from 'react';
import {Component} from "react";
import {Link} from 'react-router-dom';

import './style.scss';

class Header extends Component {
    render() {
        return (
            <nav className="navbar grey-bottom-border">
                <div className="navbar-menu">
                    <div className="navbar-start">
                        <Link to='/' className="navbar-item">Home</Link>
                        <Link to='/album' className="navbar-item">Album</Link>
                        <Link to='/personal' className="navbar-item">Personal Page</Link>
                    </div>
                </div>
            </nav>
        )
    }
}

export default Header;
