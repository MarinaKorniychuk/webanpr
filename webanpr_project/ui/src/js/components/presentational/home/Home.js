import React from 'react';
import {Component} from "react/lib/ReactBaseClasses";

class Home extends Component {
    render() {
        return (
            <section>
                <p>Hello, everyone!</p>
            </section>
        )
    }
}

export default Home;
