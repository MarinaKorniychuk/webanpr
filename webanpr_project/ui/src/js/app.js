import React from 'react';
import {Component} from "react";
import ReactDOM from "react-dom";
import {BrowserRouter, Route} from 'react-router-dom';

import './style.scss';
import Header from './components/presentational/header/Header';
import Home from './components/presentational/home/Home';

class App extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <Header/>
                <Home/>
            </div>
        )
    }
}

ReactDOM.render((
    <BrowserRouter>
        <Route component={App}/>
    </BrowserRouter>
), document.getElementById('root'));
