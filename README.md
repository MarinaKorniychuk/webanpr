#** Web Automatic Number-Plate Recognition **#

Training with Python 3, Django REST Framework and React

* ~~create db models~~
* ~~implement authorization~~
* ~~create endpoints for main views~~
* ~~configure webpack~~
* define react components
* implement 'drag and drop the file'
* stylize elements

##** Installation **##
---

Create [virtual environment](https://virtualenv.pypa.io/en/stable/) (preferably):

```
$ virtualenv -p python3 webanpr
```

Install dependencies (make sure you are inside your virtual environment):
```
$ pip install -r requirements.txt
```

Set up database schema:
```
$ python manage.py migrate
```

Create superuser:
```
$ python manage.py createsuperuser
```

##** Frontend Setting **##
---
Install Node.js:
```
$ curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
$ sudo apt-get install -y nodejs
```

Go to ```ui``` folder:
```
$ cd webanpr_project/ui
```

Install nodejs dependencies:
```
$ npm install
```

Build frontend:
```
$ npm run build
```

To watch files and recompile whenever they change:
```
$ npm run watch
```

##** Running **##

---
Run server:
```
$ python manage.py runserver
```

Go to [http://localhost:8000](http://localhost:8000) and start using app.